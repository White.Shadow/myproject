<?php
  session_start();
 ?>

<!DOCTYPE html>
<html>
   <head>
      <title>Profile Card</title>
      <link rel="stylesheet" type="text/css" href="css/profile.css">
   </head>
   <body>
      <div class="card-container">
         <div class="upper-container">
            <div class="image-container">
               <img src="img/profile.jpg" />
            </div>
         </div>
         <div class="lower-container">
            <div>
               <h3><?=$_SESSION['user']['full_name']?></h3>
               <h4><?=$_SESSION['user']['email']?></h4>
            </div>
            <div>
               <p>
                 Ваш логин: <?=$_SESSION['user']['login']?>
               </p>
            </div>
            <div>
               <a href="/home.php" class="btn">Перейти на главную</a>
            </div>
            <div style="margin-top:20px;">
              <form action="vendor/exit.php" method='post'>
                  <p><button class="btn" type="submit">Выйти из аккаунта</button></p>
                </form>
            </div>
         </div>
      </div>
   </body>
</html>
