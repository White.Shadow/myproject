extern printf

SECTION .data
    fmt_before_inverting:	  db "Before inverting.", 10, 0
    fmt_after_inverting:	  db "After inverting.", 10, 0
    fmt_element_array:    db "%d", 10, 0
    number_elements:      dq  20  ; память для 10 элементов
    array_word             dw  0, 1, 2, 3, 4, 5, 6, 7, 8, 9

	
SECTION .text
GLOBAL	main
main:
	push    rbp                     ; сохраняем стек для корректного вывода
    ; вывод сообщения Before inverting.
    mov rdi, fmt_before_inverting
    xor rax, rax
    call printf
    pop rbp

    ; вывод массива
    mov rbx, array_word
    xor rcx, rcx
    start_print_array:
        mov rdx, [rbx+rcx]          ; помещяем элемент массива в регистр rdx
        and rdx, 0x000000000000ffff ; для работы с 64 битными регистрами

        mov	rdi, fmt_element_array  ; первый аргумент - форматная строка
        mov	rsi, rdx                ; второй аргумент - сам элемент массива    
        xor rax, rax
        push rcx                    ; ; защита от перезатирания printf ом
        call	printf
        pop rcx                     ; ; защита от перезатирания printf ом

        add rcx, 2
        cmp rcx, [number_elements]
        jne start_print_array

    ; вывод сообщения After inverting.
    push rbp
    mov rdi, fmt_after_inverting
    xor rax, rax
    call printf
    pop rbp

    mov ebx, array_word
    xor r15, r15                    ; счетчик
    xor r13, r13                    ; для понимания какой элемент четный
    start_handling_array:
        mov r14, [rbx+r15]          ; помещяем элемент массива в регистр r14
        and r14, 0x000000000000ffff ; для работы с 64 битными регистрами

        xor rdx, rdx
        mov rax, r13
        mov rcx, 2
        div rcx
        cmp rdx, 0
        je is_not_odd
        jmp is_odd

        is_not_odd:
            xor r14, 0x00000000000000ff ; если четный элемент, то инвертируем биты

        is_odd:

        mov	rdi, fmt_element_array  ; первый аргумент - форматная строка
        mov	rsi, r14                ; второй аргумент - сам элемент массива    
        xor rax, rax
        push r15                    ; защита от перезатирания printf ом
        call	printf
        pop r15                     ; защита от перезатирания printf ом

        add r15, 2                  ; инкремент индекса элемента массива
        add r13, 1                  ; инкремент индекса для проверки четности индекса элемента массива
        cmp r15, [number_elements]
        jne start_handling_array

    ; выход из программы
    mov     rax, 60
    xor     rdi, rdi
    syscall
