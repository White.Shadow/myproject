﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Решето_Аткина
{
    public partial class Realization : Form
    {
        int vvod, i, j;
        public Realization()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.Invalidate();  //Создание на картинке редактора чтоб можно было выводить (4строчки)
            pictureBox1.Size = new Size(0, 0);
            Controls.Add(pictureBox1);
            Graphics tablicha = Graphics.FromImage(pictureBox1.Image);
            tablicha.Clear(Color.White);
            label2.Font = new Font("Times new roman", 14, FontStyle.Bold);
            label1.Font = new Font("Times new roman", 14, FontStyle.Bold);
            Rectangle rectangle = new Rectangle(0, 0, 320, 230);
            var pic = (Bitmap)pictureBox1.Image;
            pictureBox1.Image = pic.Clone(rectangle, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            pictureBox1.Invalidate();  //Создание на картинке редактора чтоб можно было выводить (4строчки)
            Controls.Add(pictureBox1);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
    
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs qw)
        {
            if (qw.KeyChar != 8 && (qw.KeyChar < 48 || qw.KeyChar > 57))
                qw.Handled = true;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
             textBox1.MaxLength = 3;
        }
        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            Rectangle rectangle = new Rectangle(0, 0, 320, 230);
            var pic = (Bitmap)pictureBox1.Image;
            pictureBox1.Image = pic.Clone(rectangle, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            pictureBox1.Invalidate();  //Создание на картинке редактора чтоб можно было выводить (4строчки)
            Controls.Add(pictureBox1);
            Graphics tablicha = Graphics.FromImage(pictureBox1.Image);
            tablicha.Clear(Color.White);

            while (true)
            {
                if (textBox1.Text == "")
                {
                    MessageBox.Show( "Вы ничего не ввели!");
                    tablicha.Clear(Color.White);
                    break;
                }
                else
                {
                    label1.Text = "Решето Аткина:";
                    vvod = Convert.ToInt32(textBox1.Text);
                    if (Convert.ToInt32(textBox1.Text) < 100)
                        pictureBox1.Size = new Size(310, 2 * ((Convert.ToInt32(textBox1.Text) - 1) / 10) * 10 + 30);
                    else pictureBox1.Size = new Size(310, 210);
                    break;
                }
            }
            Algoritm example = new Algoritm();
            int[] result = example.F(vvod);
            int asdf = 1;
            int asdf1 = 0;
            for (int i = 1; i <= vvod; i++)
            {
                if ((result[i] == 0) && (i < 4))
                {

                    tablicha.DrawString(i.ToString(), new Font("Arial", 13), Brushes.Red, new PointF((i - 1) % 10 * 30, (asdf1 + asdf) * 20 - 10));
                }
                else if ((result[i] == 1) && (i-1) % 10 != 0)
                {
                    
                    tablicha.DrawString(i.ToString(), new Font("Arial", 13), Brushes.Red, new PointF((i-1)%10*30, (asdf1+asdf)*20-10));
                }
                else if ((result[i] == 1) && (i - 1) % 10 == 0)
                {
                    asdf += 1;
                 
                    tablicha.DrawString(i.ToString(), new Font("Arial", 13), Brushes.Red, new PointF( 0,  (asdf1+asdf)*20-10));
                }
                else if ((result[i] == 0) && (i - 1) % 10 != 0)
                {
                   
                    tablicha.DrawString(i.ToString(), new Font("Arial", 13), Brushes.Black, new PointF((i-1)%10*30, (asdf1 + asdf) * 20-10));
                }
                else if ((result[i] == 0) && (i - 1) % 10 == 0)
                {
                    asdf1++;
                    tablicha.DrawString(i.ToString(), new Font("Arial", 13), Brushes.Black, new PointF(0, (asdf1 + asdf) * 20-10));
                }
            }
            
            

        }

        private void elementHost1_ChildChanged(object sender, System.Windows.Forms.Integration.ChildChangedEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (pictureBox1.Image !=null)
            {
                SaveFileDialog picture = new SaveFileDialog();
                picture.Title = "Сохранить картинку как:";
                picture.OverwritePrompt = true;
                picture.CheckPathExists = true;
                picture.Filter = "Image Files(*.JPG)|*.JPG";
                picture.ShowHelp = true;

                if (picture.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        pictureBox1.Image.Save(picture.FileName);
                    }
                    catch 
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка", MessageBoxButtons.OK);
                     
                    }
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Решето Аткина - Алгоритм нахождения простых чисел. Требуется ввести число, до которого необходимо найти простые числа. На рисунке простые числа будут отображаться красным цветом.");
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void textBox2_MouseClick(object sender, MouseEventArgs e)
        {
            
        }
    }
}
