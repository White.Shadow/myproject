﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace RabbitMQ.Consumer
{
    static class Program
    {
        static void Main(string[] args)
        {
            var Factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672")
            };
            using var connection = Factory.CreateConnection();
            using var chanell = connection.CreateModel();
            // объявление очереди
            chanell.QueueDeclare("demo-queue",
                durable: true,
                exclusive:false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(chanell);
            consumer.Received += (sender, e) =>
              {
                  var body = e.Body.ToArray();
                  var message = Encoding.UTF8.GetString(body);
                  Console.WriteLine(message);
              };

            chanell.BasicConsume("demo-queue", true, consumer);
            Console.ReadLine();
        }
    }
}
