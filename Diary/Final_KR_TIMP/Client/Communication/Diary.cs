﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Communication
{
    public partial class Diary : Form
    {
        string ID;
        ConnectClient connectClient = new ConnectClient();
        public Diary(string id)
        {
            ID = id;
            InitializeComponent();
        }

        private void Diary_Load(object sender, EventArgs e)
        {
            textBox2.ScrollBars = ScrollBars.Both;
            string Message = connectClient.ConServer("5|5" + ID);
            string zagl = "";
            for (int i = 0; i < Message.Length; i++) 
            {
                if (Message[i] != '|')
                    zagl += Message[i];
                else
                {
                    checkedListBox1.Items.Add(zagl);
                    zagl = "";
                }

            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int indice = checkedListBox1.SelectedIndex;
            string TimeNow = DateTime.Now.Day.ToString("00") + ":" + DateTime.Now.Month.ToString("00") + ":" + DateTime.Now.Year.ToString("00");
            int i = 0, Day, Month, Year;
            string TimeOld = "";
            if (indice != -1)
            {
                string Data = checkedListBox1.Items[indice].ToString();
                textBox2.Clear();
                label5.Text = "Осталось:";
                string Message = connectClient.ConServer("5|3" + ID + "|" + Data);
                if (Message == "false")
                    textBox2.Text += "Нет описания!";
                else if (Message != "0")
                {
                    while (Message[i] != '|')
                    {
                        textBox2.Text += Message[i];
                        i++;
                    }
                    while (Message[i+1] != ' ')
                    {
                        TimeOld += Message[i+1];
                        i++;
                    }
                    Day = Convert.ToInt32(TimeOld.Substring(0, 2)) - Convert.ToInt32(TimeNow.Substring(0, 2));
                    Month = Convert.ToInt32(TimeOld.Substring(3, 2)) - Convert.ToInt32(TimeNow.Substring(3, 2));
                    Year = Convert.ToInt32(TimeOld.Substring(6, 2)) - Convert.ToInt32(TimeNow.Substring(6, 2));
                    if(Day<0)
                    {
                        Day += 30;
                        Month--;
                    }
                    if (Month < 0)
                    {
                        Month += 12;
                        Year--;
                    }
                    if (Year < 0 || (Month == 0 && Year == 0 && Day == 0))
                    {
                        label5.ForeColor = Color.Red;
                        label5.Text = "Просрочено! Срок был до" + TimeOld;
                    }
                    else
                    {
                        label5.ForeColor = Color.Black;
                        label5.Text += Day.ToString() + "дн. " + Month.ToString() + "м. " + Year.ToString() + "г.";
                    }

                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Data = dateTimePicker1.Value.ToString();
            if (textBox1.Text != "")
            {
                checkedListBox1.Items.Add(textBox1.Text);

                connectClient.ConServer("5|4" + ID + "|" + textBox1.Text + "|" + textBox2.Text + "|" + Data);
                textBox1.Clear();
                textBox2.Clear();
                MessageBox.Show("Данные сохранены!");
            }
            else MessageBox.Show("Введите Заголовок!");
        }
            private void label3_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar == '|'))
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar == '|'))
            {
                e.Handled = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
