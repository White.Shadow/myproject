﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Text;

namespace RabbitMQ_producer
{
    static class Program
    {
        static void Main(string[] args)
        {
            var Factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672")
            };
            using var connection = Factory.CreateConnection();
            using var chanell = connection.CreateModel();
            // объявление очереди
            chanell.QueueDeclare("demo-queue",
                durable: true,
                exclusive:false,
                autoDelete: false,
                arguments: null);
            // сообщение для очереди
            var message = new { Name = "Produser", Message = "hello!" };
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));

            chanell.BasicPublish("", "demo-queue", null, body);
        }
    }
}
