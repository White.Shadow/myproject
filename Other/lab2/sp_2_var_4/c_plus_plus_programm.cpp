#include <iostream>
#include <limits.h>

using namespace std;

// Задача: дан массив из 10 слов. Инвертировать биты младших байтов
// четных элементов массива.
int main()
{
    unsigned short digits[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int digits_length = sizeof(digits)/2;

    cout << "Before inverting." << endl;
    for (int i = 0; i < digits_length; i++)
    {
        cout << digits[i] << endl;
    }

    cout << "After inverting" << endl;
    for (int index_digit = 0; index_digit < digits_length; index_digit++)
    {
        if (index_digit % 2 == 0)
        {
            unsigned short after_inverting = digits[index_digit] ^ 0xFF;
            cout << after_inverting << endl;
            continue;
        }
        cout << digits[index_digit] << endl;
        
    }
    

    return 0;
}
