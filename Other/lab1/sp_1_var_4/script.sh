#!/bin/bash

echo "Автор - Заур Велибеков"
echo "Название программы - Репозинатор"
echo "Данная программа позволяет более гибко взаимодействовать с git-репозиториями"

installed_repositories=$(yum repolist enabled)
echo "Список настроенных репозиториев с указанием подключенных."
echo "$installed_repositories"
echo -e "\n\n"

while :
do
    if [[ "$name_repo" = "" ]]
        echo "Введите имя репозитория: "
        then read name_repo
        echo -e "\n"
    fi

    repo_info=$(yum repolist $name_repo)
    is_exist_repo=$(echo "$repo_info" | grep "repolist: 0" | xargs)
    echo $is_exist_repo
    if [[ "$is_exist_repo" = "" ]]; then
        echo "Информация по репозиторию: "
        echo $(yum repolist "$name_repo")
        echo -e "\n\n"

    else
        echo -e "Данный репозиторий не существует.\n"
        echo -e "Введите информацию для создания репозитория.\n"
        echo "Введите имя для репозитория (rep_name): "
        read rep_name
        echo -e "\n"
        echo "Введите ссылку на репозиторий (baseurl): "
        read baseurl
        echo -e "\n"
        touch "/etc/yum.repos.d/CentOS-$rep_name.repo"
        
        echo "[$rep_name]" >> "/etc/yum.repos.d/CentOS-$rep_name.repo"
        echo "name=$rep_name" >> "/etc/yum.repos.d/CentOS-$rep_name.repo"
        echo "baseurl=$baseurl" >> "/etc/yum.repos.d/CentOS-$rep_name.repo"
        echo "Файл - /etc/yum.repos.d/CentOS-$rep_name.repo был создан."

        continue
    fi

    echo "Продолжить поиск репозиториев? (y/N)"
    read is_new_file
    if [[ "$is_new_file" = "N" ]]; then
        break
    fi

done
