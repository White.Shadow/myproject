<!DOCTYPE HTML>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<link href="https://fonts.googleapis.com/css?family=Montserrat:900|Roboto:300&display=swap&subset=cyrillic" rel="stylesheet">

<link rel="stylesheet" href="../css/menu.css">
<link rel="stylesheet" href="../css/main.css">

  <title>Авторизация</title>

  <style type="text/css">

  body::-webkit-scrollbar {
    width: 0px;               /* ширина всей полосы прокрутки */
  }

</style>
 </head>
 <body>
  <!-- Меню -->

  <!-- section-catalog -->
<section class="section section-catalog">
  <div class="container">
    <header class="section__header">
      <h2 class="section__title section__title--accent">Меню</h2>
      <nav class="catalog-nav">
        <ul class="catalog-nav__wrapper">
          <li class="catalog-nav__item">
            <button class="catalog-nav__btn is-active" type="button">все</button>
          </li>
          <li class="catalog-nav__item">
            <button class="catalog-nav__btn" type="button">грибные</button>
          </li>
          <li class="catalog-nav__item">
            <button class="catalog-nav__btn" type="button">мясные</button>
          </li>
          <li class="catalog-nav__item">
            <button class="catalog-nav__btn" type="button">сырные</button>
          </li>
        </ul>
      </nav >
    </header>

    <div class="catalog">
      <div class="catalog__item">
        <div class="product catalog__product">
          <img src="../img/menu/1.png" alt="" class="product__img">
          <div class="product__content">
            <h3 class="product__title">Салями</h3>
            <p class="product__description">Салями, картофель и морковь, огурцы маринованные, моцарелла, цыпленок, ветчина и французский соус </p>
          </div>
          <footer class="product__footer">
            <div class="product__sizes">
              <button class="product__size is-active" type="button">35см</button>
              <button class="product__size" type="button">30см</button>
              <button class="product__size" type="button">25см</button>
            </div>
            <div class="product__bottom">
              <div class="product__price">
                <span class="product__price-value">850</span>
                <span class="product__currency">&#8381;</span>
              </div>
              <button class="btn product__btn" type="button">заказать</button>
            </div>
          </footer>
        </div>
      </div>
      <div class="catalog__item">
        <div class="product catalog__product">
          <img src="../img/menu/2.png" alt="" class="product__img">
          <div class="product__content">
            <h3 class="product__title">Хит</h3>
            <p class="product__description">Картофель и морковь, огурцы
              маринованные, моцарелла, цыпленок,
              ветчина и французский соус </p>
          </div>
          <footer class="product__footer">
            <div class="product__sizes">
              <button class="product__size is-active" type="button">35см</button>
              <button class="product__size" type="button">30см</button>
              <button class="product__size" type="button">25см</button>
            </div>
            <div class="product__bottom">
              <div class="product__price">
                <span class="product__price-value">950</span>
                <span class="product__currency">&#8381;</span>
              </div>
              <button class="btn product__btn" type="button">заказать</button>
            </div>
          </footer>
        </div>
      </div>
      <div class="catalog__item">
        <div class="product catalog__product">
          <img src="../img/menu/3.png" alt="" class="product__img">
          <div class="product__content">
            <h3 class="product__title">Ветчина-грибы</h3>
            <p class="product__description">Салями, картофель и морковь, огурцы
              маринованные, моцарелла, цыпленок,
              ветчина и французский соус </p>
          </div>
          <footer class="product__footer">
            <div class="product__sizes">
              <button class="product__size is-active" type="button">35см</button>
              <button class="product__size" type="button">30см</button>
              <button class="product__size" type="button">25см</button>
            </div>
            <div class="product__bottom">
              <div class="product__price">
                <span class="product__price-value">880</span>
                <span class="product__currency">&#8381;</span>
              </div>
              <button class="btn product__btn" type="button">заказать</button>
            </div>
          </footer>
        </div>
      </div>
      <div class="catalog__item">
        <div class="product catalog__product">
          <img src="../img/menu/4.png" alt="" class="product__img">
          <div class="product__content">
            <h3 class="product__title">Карбонара</h3>
            <p class="product__description">Салями, картофель и морковь, огурцы маринованные, моцарелла, цыпленок, ветчина и французский соус </p>
          </div>
          <footer class="product__footer">
            <div class="product__sizes">
              <button class="product__size is-active" type="button">35см</button>
              <button class="product__size" type="button">30см</button>
              <button class="product__size" type="button">25см</button>
            </div>
            <div class="product__bottom">
              <div class="product__price">
                <span class="product__price-value">1250</span>
                <span class="product__currency">&#8381;</span>
              </div>
              <button class="btn product__btn" type="button">заказать</button>
            </div>
          </footer>
        </div>
      </div>
      <div class="catalog__item">
        <div class="product catalog__product">
          <img src="../img/menu/5.png" alt="" class="product__img">
          <div class="product__content">
            <h3 class="product__title">Фирменная</h3>
            <p class="product__description">Томатно-сливочный соус, бекон, моцарелла</p>
          </div>
          <footer class="product__footer">
            <div class="product__sizes">
              <button class="product__size is-active" type="button">35см</button>
              <button class="product__size" type="button">30см</button>
              <button class="product__size" type="button">25см</button>
            </div>
            <div class="product__bottom">
              <div class="product__price">
                <span class="product__price-value">890</span>
                <span class="product__currency">&#8381;</span>
              </div>
              <button class="btn product__btn" type="button">заказать</button>
            </div>
          </footer>
        </div>
      </div>
      <div class="catalog__item">
        <div class="product catalog__product">
          <img src="../img/menu/6.png" alt="" class="product__img">
          <div class="product__content">
            <h3 class="product__title">Ассорти</h3>
            <p class="product__description">Картофель моцарелла, цыпленок, грибы,
              ветчина и французский соус </p>
          </div>
          <footer class="product__footer">
            <div class="product__sizes">
              <button class="product__size is-active" type="button">35см</button>
              <button class="product__size" type="button">30см</button>
              <button class="product__size" type="button">25см</button>
            </div>
            <div class="product__bottom">
              <div class="product__price">
                <span class="product__price-value">850</span>
                <span class="product__currency">&#8381;</span>
              </div>
              <button class="btn product__btn" type="button">заказать</button>
            </div>
          </footer>
        </div>
      </div>
      <div class="catalog__item">
        <div class="product catalog__product">
          <img src="../img/menu/7.png" alt="" class="product__img">
          <div class="product__content">
            <h3 class="product__title">Л-01</h3>
            <p class="product__description">Томаты, моцарелла, цыпленок,
              ветчина и французский соус, секректный ингредиент </p>
          </div>
          <footer class="product__footer">
            <div class="product__sizes">
              <button class="product__size is-active" type="button">35см</button>
              <button class="product__size" type="button">30см</button>
              <button class="product__size" type="button">25см</button>
            </div>
            <div class="product__bottom">
              <div class="product__price">
                <span class="product__price-value">920</span>
                <span class="product__currency">&#8381;</span>
              </div>
              <button class="btn product__btn" type="button">заказать</button>
            </div>
          </footer>
        </div>
      </div>
      <div class="catalog__item">
        <div class="product catalog__product">
          <img src="../img/menu/8.png" alt="" class="product__img">
          <div class="product__content">
            <h3 class="product__title">Три сыра</h3>
            <p class="product__description">Чеддер, камамбер, эдам</p>
          </div>
          <footer class="product__footer">
            <div class="product__sizes">
              <button class="product__size is-active" type="button">35см</button>
              <button class="product__size" type="button">30см</button>
              <button class="product__size" type="button">25см</button>
            </div>
            <div class="product__bottom">
              <div class="product__price">
                <span class="product__price-value">990</span>
                <span class="product__currency">&#8381;</span>
              </div>
              <button class="btn product__btn" type="button">заказать</button>
            </div>
          </footer>
        </div>
      </div>
      <div class="catalog__item">
        <div class="product catalog__product">
          <img src="../img/menu/9.png" alt="" class="product__img">
          <div class="product__content">
            <h3 class="product__title">Мясная</h3>
            <p class="product__description">Курица, говядина, сливочный соус, салями,
              огурцы маринованные, томаты, грибы,
              бекон</p>
          </div>
          <footer class="product__footer">
            <div class="product__sizes">
              <button class="product__size is-active" type="button">35см</button>
              <button class="product__size" type="button">30см</button>
              <button class="product__size" type="button">25см</button>
            </div>
            <div class="product__bottom">
              <div class="product__price">
                <span class="product__price-value">950</span>
                <span class="product__currency">&#8381;</span>
              </div>
              <button class="btn product__btn" type="button">заказать</button>
            </div>
          </footer>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.section-catalog -->


 </body>
</html>
