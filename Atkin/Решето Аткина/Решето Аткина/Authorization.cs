﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Решето_Аткина
{

    public partial class Authorization : Form
    {
        public static SQLiteConnection DB;
        public Authorization()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            DB = new SQLiteConnection("Data Source=qw.db; Version=3");
            try
            {
                DB.Open();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show((ex.Message));
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void Auth_Click(object sender, EventArgs e)
        {
            if (new User(textBoxlog.Text, textBoxpasswd.Text).CheckUser())
            {
                this.Hide();
                var formMain = new Realization();
                formMain.Closed += (s, args) => this.Close();
                formMain.Show();
            }
            else
            {
                label1.Text = "Вы ввели неправильный Логин или Пароль!"; ;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            var formMain = new Registration();
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
