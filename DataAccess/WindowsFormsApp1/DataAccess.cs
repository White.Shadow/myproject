﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Dataaccess
{
    public partial class DataAccess : Form
    {
        private String DriveName = "";
        private String FilePath = "";
        private String CopyPath = "";
        private String CutPath = "";
        private String CutCopName = "";
        private int IdPath = -1;
        public int IdUser = 0;
        private string id_UserName = "";
        private string readfile = "";
        private string Access = "";

        private readonly NpgsqlConnection conn;

        List<string> PathMemory = new List<string>();

        public DataAccess(int Id, NpgsqlConnection conn)
        {
            IdUser = Id;
            this.conn = conn;
            InitializeComponent();
            DriverDefault();
        }

        private void DriverDefault()
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();
          
            foreach (DriveInfo d in allDrives)
            {
                 if(d.DriveType.ToString() == "Fixed")
                    listView2.Items.Add("Локальный диск " + d.Name);
                 else listView2.Items.Add("Внешний диск " + d.Name);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void textbox2_click(object sender, EventArgs e)
        {
            if (textBox2.Text == "⌕ поиск по разделу")
                textBox2.Clear();
            textBox2.ForeColor = Color.Black;
        }


        private void leave_textbox2(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                textBox2.ForeColor = Color.FromArgb(64, 64, 64);
                textBox2.Text = "⌕ поиск по разделу";
                DirInfo(FilePath);
            }
        }

        private void leave_textbox1(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.ForeColor = Color.FromArgb(64,64,64);
                textBox1.Text = "Выберите диск";
            }
        }

        private void textbox1_click(object sender, EventArgs e)
        {
            if (textBox1.Text == "Выберите диск")
                textBox1.Clear();
            textBox1.ForeColor = Color.Black;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Authorization().Show();
        }

        private void DataAccess_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }   

        private void DirInfo(string ItemText)
        {
            try
            {
                if (Directory.Exists(ItemText))
                {

                    string[] allfiles = Directory.GetFiles(ItemText);
                    string[] allfolders = Directory.GetDirectories(ItemText);
                    FilePath = ItemText + @"\";
                    listView1.Items.Clear();

                    foreach (string filename in allfiles)
                    {
                        DirectoryInfo directory = new DirectoryInfo(filename);
                        if ((directory.Attributes & FileAttributes.Hidden) == 0)
                        {
                            ListViewItem fileinfo = new ListViewItem(Path.GetFileName(filename));
                            var info  = new FileInfo(filename);
                            DateTime dt = File.GetLastWriteTime(filename);
                            fileinfo.SubItems.Add(dt.ToString("dd.MM.yyyy HH:mm"));
                            fileinfo.SubItems.Add("Файл " + Path.GetExtension(filename));
                            fileinfo.SubItems.Add((info.Length/1024/1024).ToString() + " Мб");
                            listView1.Items.Add(fileinfo);
                            FilePath = filename.Replace(Path.GetFileName(filename), "");
                        }

                    }
                    foreach (string folder in allfolders)
                    {
                        DirectoryInfo directory = new DirectoryInfo(folder);
                        if ((directory.Attributes & FileAttributes.Hidden) == 0)
                        {
                            ListViewItem fileinfo2 = new ListViewItem(Path.GetFileName(folder));
                            var info = new FileInfo(folder);
                            DateTime dt = File.GetLastWriteTime(folder);
                            fileinfo2.SubItems.Add(dt.ToString("dd.MM.yyyy HH:mm"));
                            fileinfo2.SubItems.Add("Папка");
                            fileinfo2.SubItems.Add(" ");
                            listView1.Items.Add(fileinfo2);
                            FilePath = folder.Replace(Path.GetFileName(folder), "");
                        }
                    }

                    textBox1.ForeColor = Color.Black;
                    textBox1.Text = FilePath;

                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Файл или папка не загружается! Код ошибки: " + e.ToString(), "Ошибка!");
            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView2.SelectedItems)
            {
                DriveName = item.Text;
                DriveName = DriveName.Replace("Локальный диск ","");
                DriveName = DriveName.Replace("Внешний диск ", "");
                listView1.Items.Clear();

                DirInfo(DriveName);

                TruncateFileList();
            }
        }

        private void TruncateFileList()
        {
            if (IdPath + 1 < PathMemory.Count)
            {
                PathMemory.RemoveRange(IdPath + 1, PathMemory.Count - IdPath - 1);
            }
            if (IdPath == -1 || PathMemory[IdPath] != FilePath) 
            {
                IdPath++;
                PathMemory.Add(FilePath);
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            int i = 0, count = 0;
            string temp = FilePath;
            var p = "\\";
            while ((i = FilePath.IndexOf(p, i)) != -1) 
            { 
                ++count; 
                i += p.Length; 
            }
            if (count == 0)
            {
                return;
            }

            FilePath = FilePath.Substring(0, FilePath.Length - 1);
            if (count > 2)
            {
                FilePath = FilePath.Remove(FilePath.LastIndexOf('\\'));
            }
            else if (count == 2)
            {
                FilePath = FilePath.Remove(FilePath.LastIndexOf('\\') + 1);
            }
            else FilePath += "\\";
            if (temp != FilePath)
            {
                DirInfo(FilePath);

                TruncateFileList();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (IdPath + 1 < PathMemory.Count)
            {
                IdPath++;
                DirInfo(PathMemory[IdPath]);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (IdPath - 1 >= 0)
            {
                IdPath--;
                DirInfo(PathMemory[IdPath]);
            }
        }

        private void Textbox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                string SearhPath = textBox1.Text;

                if (SearhPath != "" && SearhPath != FilePath)
                {
                    DirInfo(SearhPath);
                    TruncateFileList();
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            foreach (ListViewItem item in listView1.SelectedItems)
            {
                CutPath = "";
                CopyPath = FilePath + item.Text;
                CutCopName = item.Text;
            }
            if (CopyPath != "")
                MessageBox.Show("Скопировано");
            else
                MessageBox.Show("Выберите элемент", "Ошибка");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.SelectedItems)
            {
                try
                {
                    if (File.Exists(FilePath + item.Text) && (Path.GetExtension(item.Text) == ".txt" || Path.GetExtension(item.Text) == ".exe"))
                    {
                        var command = new NpgsqlCommand();
                        command.Connection = conn;
                        command.CommandText = $"select * from auth where id = '{IdUser}'";
                        NpgsqlDataReader read = command.ExecuteReader();
                        while (read.Read())
                        {
                            id_UserName = read.GetString(1);
                        }
                        read.Close();

                        var command_2 = new NpgsqlCommand();
                        command_2.Connection = conn;
                        command_2.CommandText = $"select * from useraccess where id_username = '{id_UserName}' and filename = '{FilePath + item.Text}'";
                        NpgsqlDataReader read_2 = command_2.ExecuteReader();
                        int check = 0;

                        while (read_2.Read())
                        {
                            check = 1;
                            if (Path.GetExtension(FilePath + item.Text) == ".txt")
                            {
                                if (read_2.GetString(1) == "True" && read_2.GetString(2) == "False")
                                {
                                    readfile = "True";
                                    this.Hide();
                                    new OpenFiles(conn, IdUser, FilePath + item.Text, readfile).Show();
                                }
                                else if (read_2.GetString(4) == "True" || (read_2.GetString(1) == "False" && read_2.GetString(2) == "False"))
                                {
                                    Access = "True";
                                    MessageBox.Show("Невозможно открыть!Нет прав доступа!", "Ошибка открытия!");
                                }
                                else
                                {
                                    this.Hide();
                                    new OpenFiles(conn, IdUser, FilePath + item.Text, readfile).Show();
                                }
                            }
                            else if (Path.GetExtension(FilePath + item.Text) == ".exe")
                            {
                                if (read_2.GetString(3) == "True")
                                {
                                    try
                                    {
                                        System.Diagnostics.Process.Start(FilePath + item.Text);
                                    }
                                    catch { }

                                }
                                else MessageBox.Show("Невозможно открыть исполняемый файл!", "Нет прав доступа!");

                            }
                        }

                        if (check == 0)
                        {
                            if (Path.GetExtension(FilePath + item.Text) == ".txt")
                            {
                                this.Hide();
                                new OpenFiles(conn, IdUser, FilePath + item.Text, readfile).Show();
                            }
                            else if (Path.GetExtension(FilePath + item.Text) == ".exe")
                            {
                                try
                                {
                                    System.Diagnostics.Process.Start(FilePath + item.Text);
                                }
                                catch { }
                            }
                        }
                        read_2.Close();
                    }
                    else MessageBox.Show("Не является текстовым либо исполняемым файлом!");
                }
                catch
                {
                    MessageBox.Show("Система выдало ошибку!Возможно ограничение со стороны ОС!", "Ошибка!");
                }
            }

        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.SelectedItems)
            {
                DriveName = item.Text;
                DirInfo(FilePath + DriveName);

                TruncateFileList();
            }
        }


        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)      
            {
                DirInfo(FilePath);
                string SearhPath = textBox2.Text;

                if (SearhPath != "")
                {
                    foreach (ListViewItem itm in listView1.Items)
                    {

                        if (itm.Text == SearhPath)
                        {
                            //пропускаем
                        }
                        else
                        {
                            listView1.Items.Remove(itm); //Удаляем лишние элементы
                        }
                    }

                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (CopyPath != "" && CopyPath != (FilePath + CutCopName))
            {
                File.Copy(CopyPath, FilePath + CutCopName);
                DirInfo(FilePath);
                MessageBox.Show("Файл успешно скопирован!");
            }
            else if (CutPath != "" && CutPath != (FilePath + CutCopName))
            {
                File.Copy(CutPath, FilePath + CutCopName);
                File.Delete(CutPath);
                DirInfo(FilePath);
                MessageBox.Show("Файл успешно перемещен!");
            }
            else if (CopyPath == (FilePath + CutCopName) || CutPath == (FilePath + CutCopName))
                MessageBox.Show("Файл с таким именем уже существует", "Ошибка");
            else
                MessageBox.Show("Выберите файл или папку для вставки!", "Ошибка!");

        }

        private void button6_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.SelectedItems)
            {
                CopyPath = "";
                CutPath = FilePath + item.Text;
                CutCopName = item.Text;
            }
            if (CutPath != "")
                MessageBox.Show("Файл готов для вставки!");
            else
                MessageBox.Show("Выберите элемент", "Ошибка");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            bool check = false;
            foreach (ListViewItem item in listView1.SelectedItems)
            {
                check = true;
                File.Delete(FilePath + item.Text);
                DirInfo(FilePath);
                MessageBox.Show("Файл успешно удален!");

            }
            if (check == false)
                MessageBox.Show("Выберите элемент", "Ошибка");
        }

        private void button15_Click(object sender, EventArgs e)
        {
            this.Hide();
            new RoleApplications(conn, IdUser).Show();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            bool check = false;
            foreach (ListViewItem item in listView1.SelectedItems)
            {
                check = true;
                this.Hide();
                new UserAccess(FilePath + item.Text, conn, IdUser).Show();

            }
            if (check == false)
                MessageBox.Show("Выберите файл или папку для выдачи прав!", "Ошибка");
        }

        private void DataAccess_Load(object sender, EventArgs e)
        {
            var command = new NpgsqlCommand();
            command.Connection = conn;
            command.CommandText = $"select * from auth where id = '{IdUser}'";
            NpgsqlDataReader read = command.ExecuteReader();
            while (read.Read())
            {
                if(read.GetString(3) == "admin" && read.GetString(4) == "true")
                {
                    button13.Visible = true;
                    button15.Visible = true;
                    label10.Visible = true;
                    label12.Visible = true;
                }
                else
                {
                    button13.Visible = false;
                    button15.Visible = false;
                    label10.Visible = false;
                    label12.Visible = false;
                }
            }
            read.Close();
        }
    }
}
