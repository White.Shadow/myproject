<?php
  session_start();
?>

<!DOCTYPE HTML>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styleAuth.css">
  <title>Авторизация</title>
 </head>
 <body>

  <!-- Форма регистрации -->

<form class="" action="vendor/signup.php" method="post">
  <h1>Регистрация</h1>
  <label>ФИО</label>
  <input type="text" name="full_name" placeholder="Введите ФИО полностью">
  <label>Логин</label>
  <input type="text" name="login" placeholder="Введите свой логин">
  <label>Почта</label>
  <input type="text" name="email" placeholder="Введите почту(незабудьте обязательный символ @)">
  <label>Пароль</label>
  <input type="password" name="password" placeholder="Введите пароль">
  <label>Подтверждение пароля</label>
  <input type="password" name="password_confirm" placeholder="Введите пароль повторно">
  <button type="submit" >Войти</button>
  <p>
    <font size="4" color="#fa8e47">Уже есть аккаунт? </font><a href="/auth.php">авторизуйтесь</a>!
  </p>
  <div class="GotoHome" align="center">
        <a href="/"> <img style="vertical-align: middle;" src="img\home.png" alt="waiting..">Перейти на главную</a>
  </div>
    <?php
      if($_SESSION['message']) {
        echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Ошибка! </strong>' . $_SESSION['message'] . '  </div>';
      }
      unset($_SESSION['message']);
    ?>
  <script>
  var close = document.getElementsByClassName("closebtn");
  var i;

  for (i = 0; i < close.length; i++) {
    close[i].onclick = function(){
      var div = this.parentElement;
      div.style.opacity = "0";
      setTimeout(function(){ div.style.display = "none"; }, 600);
    }
  }
  </script>

</form>

 </body>
</html>
