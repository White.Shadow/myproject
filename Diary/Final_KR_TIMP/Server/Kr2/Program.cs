﻿using System;
using System.Data.SQLite;

namespace Kr2
{
    class Program
    {
        public static SQLiteConnection DB;
        static void Main(string[] args)
        {
            DB = new SQLiteConnection("Data Source=BdOrganazer.db; Version=3");
            try
            {
                DB.Open();
                Console.WriteLine("Сервер запущен! Ожидание подключений");
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message + " Error! Please Try again!");
            }
            ConnectServer connect = new ConnectServer();
            connect.Connect();
        }
    }
}
