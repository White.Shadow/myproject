<?php
  session_start();
?>

<!DOCTYPE HTML>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="styleAuth.css">
  <title>Авторизация</title>
 </head>
 <body>

  <!-- Форма авторизации -->

<form class="" action="vendor/signin.php" method="post">
  <h1>Авторизация</h1>
  <label>Логин</label>
  <input type="text" name ="login" placeholder="Введите свой логин">
  <label>Пароль</label>
  <input type="password" name="password" placeholder="Введите пароль">
  <button type="submit">Войти</button>
  <p>
    <font size="4" color="#fa8e47">У вас нет аккаунта? </font><a href="register.php"> зарегистрируйтесь</a>!
   <div class="GotoHome" align="center">
         <a href="/"> <img style="vertical-align: middle;" src="img\home.png" alt="waiting..">Перейти на главную</a>
   </div>
  </p>
  <?php
    if($_SESSION['message'] == "Регистрация прошла успешно!") {
      echo '<div class="alert success"><span class="closebtn">&times;</span><strong>Удачно! </strong>' . $_SESSION['message'] . '  </div>';
    }
    if($_SESSION['messageauth'] == "Авторизуйтесь, для заказа столика!") {
      echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Извините! </strong>' . $_SESSION['messageauth'] . '  </div>';
    }
    if($_SESSION['message'] == "Не верный логин или пароль!") {
      echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Ошибка! </strong>' . $_SESSION['message'] . '  </div>';
    }
    unset($_SESSION['message']);
  ?>
<script>
var close = document.getElementsByClassName("closebtn");
var i;

for (i = 0; i < close.length; i++) {
  close[i].onclick = function(){
    var div = this.parentElement;
    div.style.opacity = "0";
    setTimeout(function(){ div.style.display = "none"; }, 600);
  }
}
</script>
</form>

 </body>
</html>
