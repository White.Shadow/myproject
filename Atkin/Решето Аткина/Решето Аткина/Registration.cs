﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
namespace Решето_Аткина
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {
            label1.Font = new Font("Times new roman", 14, FontStyle.Bold);
        }
        private void Auth_Click(object sender, EventArgs e)
        {
            SQLiteCommand command = Authorization.DB.CreateCommand();
            command.Parameters.Add("@log", DbType.String).Value = textBoxlog.Text;
            command.Parameters.Add("@passwd", DbType.String).Value = textBoxpasswd.Text;
            command.CommandText = "select * from Пользователи where Логин like @log";
            if (command.ExecuteScalar() != null)
            {
                label3.Text = "Такой пользователь уже существует";
            }
            else if (textBoxlog.Text == "" || textBoxpasswd.Text == "")
            {
                MessageBox.Show("Введите Логин и Пароль!");
            }

            else
            {
                label3.Text = "Учетная запись создана!";
                command.CommandText = "insert into Пользователи(Логин, Пароль) values(@log, @passwd)";
                command.ExecuteNonQuery();
            };
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            var formMain = new Authorization();
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            var formMain = new Authorization();
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBoxlog_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
