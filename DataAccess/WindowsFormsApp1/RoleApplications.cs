﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dataaccess
{
    public partial class RoleApplications : Form
    {
        NpgsqlConnection conn;
        int id = 0;
        public RoleApplications(NpgsqlConnection conn, int Id)
        {
            this.conn = conn;
            this.id = Id;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count != 0)
            {
                for (int i = 0; i < checkedListBox1.CheckedItems.Count; i++)
                {
                    var command = new NpgsqlCommand();
                    command.Connection = conn;
                    command.CommandText = $"UPDATE public.auth SET checkrole = 'true' WHERE login = '{checkedListBox1.CheckedItems[i].ToString()}'; ";
                    command.ExecuteNonQuery();
                }
                MessageBox.Show("Отмеченные стали администраторами!", "Выполнено!");
                checkedListBox1.Items.Clear();
                var command2 = new NpgsqlCommand();
                command2.Connection = conn;
                command2.CommandText = $"select * from auth where checkrole = 'check'";
                NpgsqlDataReader read = command2.ExecuteReader();
                while (read.Read())
                {
                    checkedListBox1.Items.Add(read.GetString(1));
                }
                read.Close();
                if (checkedListBox1.Items.Count == 0)
                {
                    label2.Visible = true;
                }
                else label2.Visible = false;
            }
            else MessageBox.Show("Выберите пользователя!", "Ошибка!");
        }

        private void RoleApplications_Load(object sender, EventArgs e)
        {
            var command = new NpgsqlCommand();
            command.Connection = conn;
            command.CommandText = $"select * from auth where checkrole = 'check'";
            NpgsqlDataReader read = command.ExecuteReader();
            while (read.Read())
            {
                checkedListBox1.Items.Add(read.GetString(1));
            }
            read.Close();
            if (checkedListBox1.Items.Count == 0)
            {
                label2.Visible = true;
            }
            else label2.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count != 0)
            {
                for (int i = 0; i < checkedListBox1.CheckedItems.Count; i++)
                {
                    var command = new NpgsqlCommand();
                    command.Connection = conn;
                    command.CommandText = $"UPDATE public.auth SET checkrole = 'true', \"role\" = 'user' WHERE login = '{checkedListBox1.CheckedItems[i].ToString()}'; ";
                    command.ExecuteNonQuery();
                }
                MessageBox.Show("Роли отмеченным не выданы!", "Выполнено!");
                checkedListBox1.Items.Clear();
                var command2 = new NpgsqlCommand();
                command2.Connection = conn;
                command2.CommandText = $"select * from auth where checkrole = 'check'";
                NpgsqlDataReader read = command2.ExecuteReader();
                while (read.Read())
                {
                    checkedListBox1.Items.Add(read.GetString(1));
                }
                read.Close();
                if (checkedListBox1.Items.Count == 0)
                {
                    label2.Visible = true;
                }
                else label2.Visible = false;
            }
            else MessageBox.Show("Выберите пользователя!", "Ошибка!");
        }

        private void RoleApplications_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            new DataAccess(id, conn).Show();
        }
    }
}
