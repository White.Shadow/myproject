<?php
  session_start();
  if(!$_SESSION['user']) {
    $_SESSION['messageauth'] = 'Авторизуйтесь, для заказа столика!';
    header('Location: ../auth.php');
  }
?>

<!DOCTYPE html>
<html>
   <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
     <link rel="stylesheet" type="text/css" href="styleOrder.css">
      <title>Заказ столика</title>
   </head>
   <body>
     <form class="" action="vendor/tableOrder.php" method="post">
       <h1>Заказ Столика</h1>
       <div>
          <h3 style="color:white;margin-top:10px;">Ваше ФИО:<?=$_SESSION['user']['full_name']?></h3>
          <h3 style="color:white; margin-top:10px;">Ваша почта:<?=$_SESSION['user']['email']?></h3>
       </div>
       <div style="color:white; margin-top:10px;">
          <h3>
            Ваш логин: <?=$_SESSION['user']['login']?>
          </h3>
       </div>
       <p style="color:orange;">
         Напишите телефон в формате 8xxx-xxx-xx-xx:
         <input type="tel" name="tel"  pattern="8[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}">
        </p>
        <label style="color:orange;" for="start">На какой день бронировать:</label>
        <input type="date" id="start" name="trip-start"
          value="2023-01-23"
          min="2023-01-23" max="2025-12-31">
        <p style="color:orange;">На какое время заказать стол:</p>
        <p><input type="time" name="cron" value="09:00" min="09:00" max="23:00"></p>
        <p style="color:orange;"><b>Пожелания(например: столик на четверых)</b></p>
        <p><textarea style="margin-left: 7%; resize: none;" rows="5" cols="45" name="text"></textarea></p>
       <button type="submit">Забронировать</button>
        <div class="GotoHome" align="center">
              <a href="/"> <img style="vertical-align: middle;" src="img/home.png" alt="waiting..">Перейти на главную</a>
        </div>
        <?php
          if($_SESSION['message'] == "Заполните обязательные поля!") {
            echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Ошибка! </strong>' . $_SESSION['message'] . '  </div>';
          }
          unset($_SESSION['message']);
        ?>
      <script>
      var close = document.getElementsByClassName("closebtn");
      var i;

      for (i = 0; i < close.length; i++) {
        close[i].onclick = function(){
          var div = this.parentElement;
          div.style.opacity = "0";
          setTimeout(function(){ div.style.display = "none"; }, 600);
        }
      }
      </script>
      </form>
   </body>
</html>
