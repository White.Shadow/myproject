﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Xml.Linq;

namespace taskReadXMLfromURL
{
    public partial class TreeXML : Form
    {
        public TreeXML()
        {
            InitializeComponent();
        }

        private void AddNode(XmlNode inXmlNode, TreeNode inTreeNode)
        {
            XmlNode xNode;
            TreeNode tNode;
            XmlNodeList nodeList;
            int i = 0;
            if (inXmlNode.HasChildNodes)
            {
                nodeList = inXmlNode.ChildNodes;
                for (i = 0; i <= nodeList.Count - 1; i++)
                {
                    xNode = inXmlNode.ChildNodes[i];
                    inTreeNode.Nodes.Add(new TreeNode(xNode.Name));
                    tNode = inTreeNode.Nodes[i];
                    AddNode(xNode, tNode);
                }
            }
            else
            {
                inTreeNode.Text = inXmlNode.InnerText.ToString();
            }
        }

        public string URLString;
        private void Form2_Load(object sender, EventArgs e)
        {
            try
            {
                XmlDataDocument xmldoc = new XmlDataDocument();
                XmlNode xmlnode;
                xmldoc.Load(URLString);
                xmlnode = xmldoc.ChildNodes[1];
                treeView1.Nodes.Clear();
                treeView1.Nodes.Add(new TreeNode(xmldoc.DocumentElement.Name));
                TreeNode tNode;
                tNode = treeView1.Nodes[0];
                AddNode(xmlnode, tNode);
            }
            catch
            {
                MessageBox.Show("Произошла ошибка. Возможно вы ввели неправильный адрес!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            var formMain = new ReadXml();
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }
    }
}
