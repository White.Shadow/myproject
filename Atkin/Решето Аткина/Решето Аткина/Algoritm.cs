﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Решето_Аткина
{
    public class Algoritm
    {
        public int[] F(int vvod)
        {
            int x2, y2;
            int i, j;
            int n;
            int sqrt_vvod = (int)Math.Sqrt((double)vvod);
            int[] a = new int[vvod + 1];
            for (i = 0; i < vvod; i++)
                a[i] = 0;
            x2 = 0;
            for (i = 1; i <= sqrt_vvod; i++)
            {
                x2 += 2 * i - 1;
                y2 = 0;
                for (j = 1; j <= sqrt_vvod; j++)
                {
                    y2 += 2 * j - 1;

                    n = 4 * x2 + y2;
                    if ((n <= vvod) && (n % 12 == 1 || n % 12 == 5))
                        a[n] = 1;

                    n -= x2;
                    if ((n <= vvod) && (n % 12 == 7))
                        a[n] = 1;

                    n -= 2 * y2;
                    if ((i > j) && (n <= vvod) && (n % 12 == 11))
                        a[n] = 1;
                }
            }

            for (i = 5; i <= sqrt_vvod; i++)
            {
                if (a[i] == 1)
                {
                    n = i * i;
                    for (j = n; j <= vvod; j += n)
                    {
                        a[j] = 0;
                    }
                }
            }
            return a;
        }

    }

}

