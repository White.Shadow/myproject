﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dataaccess
{
    public static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Authorization());
        }

        static public void InfoRequest()
        {
            foreach (Form f in Application.OpenForms)
                if (f.Name == "Helper")
                    return;
            new Helper().Show();
        }
        static public bool CheckUser(NpgsqlConnection conn, string login)
        {
            bool check = false;
            var cmd = new NpgsqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = $"select exists(select 1 from auth where login = '{login}')";
            
            NpgsqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                check = rdr.GetBoolean(0);
            }
            rdr.Close();
            return check;
        }
    }
}
