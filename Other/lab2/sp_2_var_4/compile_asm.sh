#!/bin/bash

nasm -g -F dwarf -f elf64 -l nasm_programm.lst nasm_programm.asm

gcc -m64 -o output nasm_programm.o -no-pie
