﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace taskReadXMLfromURL
{
    public partial class ReadXml : Form
    {
        public ReadXml()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Font = new Font("Times new roman", 14, FontStyle.Bold);
            label2.Font = new Font("Times new roman", 14, FontStyle.Bold);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.Text = "";
                String URLString = textBox1.Text;
                XmlTextReader reader = new XmlTextReader(URLString);
                XDocument doc = XDocument.Load(URLString);
                richTextBox1.Text = doc.ToString();
            }
            catch
            {
                MessageBox.Show("Произошла ошибка. Возможно вы ввели неправильный адрес!");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Приложение для загрузки xml файла по ссылке. " +
                "Введите ссылку в адресной строке и нажмите загрузить.\n" +
                "Нажмите визуальное дерево, если хотите получить дерево XML.");
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
     
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            var formMain = new TreeXML();
            formMain.URLString = textBox1.Text;
            formMain.Closed += (s, args) => this.Close();
            formMain.Show();

        }
    }
}
