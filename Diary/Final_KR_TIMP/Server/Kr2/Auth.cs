﻿using System;
using System.Data;
using System.Data.SQLite;


namespace Kr2
{
    class Auth
    {

        public bool Check(string Data)
        {
            string log = "", passw = "";
            int K = 0;
            for (int i = 3; i < Data.Length - 1; i++)
            {
                if (Data[i] == '|')
                {
                    K = 1;
                }
                if (K == 0)
                    log += Data[i];
                else passw += Data[i + 1];

            }
            bool check = new User(log, passw).CheckUser();
            return check;
        }
        public string Id(string Data)
        {
            SQLiteCommand command = Program.DB.CreateCommand();
            string ID = "";
            string log = "", passw = "";
            int K = 0;
            for (int i = 3; i < Data.Length - 1; i++)
            {
                if (Data[i] == '|')
                {
                    K = 1;
                }
                if (K == 0)
                    log += Data[i];
                else passw += Data[i + 1];

            }
            command.CommandText = "select * from Авторизация where Логин like @Log and Пароль like @Passw ";
            command.Parameters.Add("@Log", DbType.String).Value = log;
            command.Parameters.Add("@Passw", DbType.String).Value = passw;
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ID += reader["Id"];
                }

            }
            return ID;
        }
    }
}
