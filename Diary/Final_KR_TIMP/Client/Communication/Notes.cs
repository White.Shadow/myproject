﻿using System;
using System.Windows.Forms;

namespace Communication
{
    public partial class Notes : Form
    {
        string ID;
        ConnectClient connectClient = new ConnectClient();

        public Notes(string id)
        {
            InitializeComponent();
            ID = id;
        }


        private void Notes_Load(object sender, EventArgs e)
        {
            string Message = connectClient.ConServer("4|5" + ID);
            if (Message == "false")
                textBox3.Text = "Нет такой записи!";
            else if (Message != "0")
                textBox3.Text += Message;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                textBox2.Clear();
                string Message = connectClient.ConServer("4|4" + ID + "|" + textBox1.Text);
                if (Message == "false")
                    MessageBox.Show("Нет такой записи!");
                else if (Message != "0")
                    textBox2.Text += Message;
                textBox3.Clear();
                Message = connectClient.ConServer("4|5" + ID);
                if (Message == "false")
                    textBox3.Text = "Нет записей!";
                else if (Message != "0")
                {
                    textBox3.Text += Message;
                }
            }
            else MessageBox.Show("Введите имя заметки!");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                if (textBox2.Text == "")
                {
                    textBox2.Text = "Описания нет!";
                }
                string Message = connectClient.ConServer("4|4" + ID + "|" + textBox1.Text);

                if (Message == "false")
                {
                    string message = connectClient.ConServer("4|3" + ID + "|" + textBox1.Text + "|" + textBox2.Text);
                    textBox3.Clear();
                    Message = connectClient.ConServer("4|5" + ID);
                    if (Message == "false")
                        textBox3.Text = "Нет записей!";
                    else if (Message != "0")
                    {
                        textBox3.Text += Message;
                    }
                    MessageBox.Show("Запись сохранена!");
                }
                else MessageBox.Show("Запись с таким именем уже сохранена!");
            }
            else MessageBox.Show("Введите имя заметки для сохранения!");
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar == '|'))
            {
                e.Handled = true;
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar == '|'))
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
        }
    }
}
