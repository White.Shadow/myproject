﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dataaccess
{
    public partial class Authorization : Form
    {
        public NpgsqlConnection conn;
        private int IdUser = 0;
        public Authorization()
        {
            InitializeComponent();
            string conn_param = "Server=127.0.0.1;Port=5432;User Id=postgres;Password=zaur;Database=accessdb";
            conn = new NpgsqlConnection(conn_param);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void Avtorization_Load(object sender, EventArgs e)
        {

            conn.Open(); //Открываем соединение.

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Login = textBox1.Text;
            string Passwd = textBox2.Text;
            if (Login == "Username" || Passwd == "Password")
            {
                MessageBox.Show("Заполните поля", "Ошибка");
            }
            else
            {
                bool check = false;
                var cmd = new NpgsqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = $"select exists(select 1 from auth where login = '{textBox1.Text}' and password = '{textBox2.Text}')";

                NpgsqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    check = rdr.GetBoolean(0);
                }
                rdr.Close();
                if (check == true)
                {
                    var command = new NpgsqlCommand();
                    command.Connection = conn;
                    command.CommandText = $"select * from auth where login = '{textBox1.Text}' and password = '{textBox2.Text}'";

                    NpgsqlDataReader read = command.ExecuteReader();

                    while (read.Read())
                    {
                         IdUser = read.GetInt32(0);
                    }
                    read.Close();
                    this.Hide();
                    new DataAccess(IdUser, conn).Show();
                }
                else
                    MessageBox.Show("Неверный логин или пароль!");
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            new Registration(conn).Show();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            Program.InfoRequest();
        }

        private void AuthFormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void textbox1_click(object sender, EventArgs e)
        {
            if(textBox1.Text == "Username")
                textBox1.Clear();
            textBox1.ForeColor = Color.Black;
        }

        private void textbox2_click(object sender, EventArgs e)
        {
            textBox2.MaxLength = 14;
            if (textBox2.Text == "Password")
                textBox2.Clear();
            textBox2.ForeColor = Color.Black;
            textBox2.PasswordChar = '*';
        }

        private void textbox1_leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {           
                textBox1.ForeColor = Color.Gray;
                textBox1.Text = "Username";
            }
        }

        private void textbox2_leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                textBox2.ForeColor = Color.Gray;
                textBox2.Text = "Password";
                textBox2.PasswordChar = '\0';
            }
            else
            {
                textBox2.ForeColor = Color.Black;
                textBox2.PasswordChar = '*';
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
