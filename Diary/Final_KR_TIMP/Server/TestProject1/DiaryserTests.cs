﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kr2;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;

namespace Kr2.Tests
{
    [TestClass()]
    public class DiaryserTests
    {
        [TestMethod()]
        public void DiaryTest()
        {
            string Data = "5|4840|fsgdfgdfg|Zaurovd|12.12.2000";
            string expected = "0";

            // act
            SQLiteConnection DB = new SQLiteConnection("Data Source=C:/Users/Zaur Velibekov/kursach_pain/Final_KR_TIMP/Server/Kr2/bin/Debug/BdOrganazer.db; Version=3");
            //SQLiteConnection DB = new SQLiteConnection("Data Source=BdOrganazer.db; Version=3");

            DB.Open();
            Diaryser diary = new Diaryser();
            string actual = diary.Diary(Data, DB);
            //assert

            Assert.AreEqual(expected, actual);
        }
    }
}