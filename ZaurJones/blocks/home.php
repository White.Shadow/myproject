<!DOCTYPE HTML>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Авторизация</title>
  <link rel="stylesheet" type="text/css" href="../css/Home.css">
    <style type="text/css">
      body::-webkit-scrollbar {
          width: 0px;               /* ширина всей полосы прокрутки */
      }
      body {
        background: black; /* А это работает */
        background: rgba(0, 0, 0, 0.5);
      }
      </style>
 </head>

 <body>
  <!-- Главная -->
  <div id="slider-wrapper">
    <input type="radio" id="button-1" name="buttons" checked="checked"/>
    <label for="button-1"></label>
    <input type="radio" id="button-2" name="buttons"/>
    <label for="button-2"></label>
    <input type="radio" id="button-3" name="buttons"/>
    <label for="button-3"></label>
    <div id="slider">
      <ul>
        <li id="slide1">
          <p style="position:absolute; font-size:12px; color:#bf0; font-weight: bold; text-align: center; line-height:1.1;">
            <span>НОВОГОДНИЕ ПОДАРКИ!<br><big>ZaurJones</big> поздравляет с новым 2023 годом и приготовил для вас
            новогодние скидки!
           </span>
           </p>
           <p style="position:absolute; margin-left: 490px; margin-top: 120px;font-size:10px;
           color:black;  font-family: Montserrat, sans-serif;  font-weight: bold;     background: rgba(255, 255, 255, 0.3);
           line-height:1; width:330px;">
             <span>Приходите 31 декабря<br> и скажите кодовую фразу <br>
               <big><big>"С новым годом, ZaurJones!"</big></big> <br>и получите скидку в 30% <br>на все блюда!
               </span>
           </p>
           <img style="position:absolute; width:300px; height:480px; margin-left:10px;" src="../img/product-2.png" alt="">
          <img src="../img/slide-2.jpg" width="100%" height="300" alt="">

          <li id="slide2">

              <img src="../img/slide-1.jpg" width="100%" height="300" alt="">

          </li>
          <li id="slide3">
                <img src="../img/slide-3.jpg" width="100%" height="300" alt="">

          </li>
      </ul>
    </div>
  </div>


   <!--Показ меню-->
  <div align="center" class='header-down'>

      <div class='header-title'>
          Добро пожаловать в

          <div class='header-subtitle'>
              Наш ресторан
          </div>

          <div class='header-suptitle'>
              ДОМ ЛУЧШЕЙ ЕДЫ
          </div>

          <div class='header-bth'>
              <a href='menu.php' class='header-button'>Показать меню</a>
          </div>

      </div>

  </div>
   <!--Карточки-->
   <div class='cards'>

       <div class='container'>

          <div class='cards-holder'>

               <div class='card'>

                   <div class='card-image'>
                       <img class='card-img' src='../img/card.png'>
                   </div>

                   <div class='card-title'>
                       Магическая  <span>Атмосфера</span>
                   </div>

                   <div class='card-desc'>
                       В нашем заведении царит магическая атмосфера наполненная вкусными ароматами
                   </div>

               </div>

               <div class='card'>

                   <div class='card-image'>
                       <img class='card-img' src='../img/card.png'>
                   </div>

                   <div class='card-title'>
                       Лучшее качество  <span>Еды</span>
                   </div>

                   <div class='card-desc'>
                       Качество нашей Еды - отменное!

                   </div>

               </div>

               <div class='card'>

                   <div class='card-image'>
                       <img class='card-img' src='../img/card.png'>
                   </div>

                   <div class='card-title'>
                      Недорогая  <span>Еда</span>
                   </div>

                   <div class='card-desc'>
                       Стоимость нашей Еды зависит только от ее количества. Качество всегда на высоте!
                   </div>

               </div>
           </div>
       </div>

   </div>
   <div class="suptitle">
        <p>Когда самое лучшее время начать?</p>
        <p>1) Самое лучшее время было вчера.</p>
        <p>2) Следующее самое лучшее время - сейчас!</p>
        <p>ПО БУДНЯМ с 9:00 до 23:00</p>
        <p>Выходные с 11:00 до 21:00</p>
   </div>
 </body>
</html>
