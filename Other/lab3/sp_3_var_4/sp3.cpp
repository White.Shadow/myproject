#include <iostream>

using namespace std;

extern "C" void create_array_and_perform_transposition();

int main()
{
    create_array_and_perform_transposition();
    return 0;
}