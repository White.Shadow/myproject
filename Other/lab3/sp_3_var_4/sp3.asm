extern printf
extern scanf


section .data
    size_array_message:             db "Size array (NxN):", 10, 0
    size_array_value_string         db "%d", 0
    message_random_array            db "========== Random array ==========", 10, 0
    message_result_array:           db "========== Array after transposition ==========", 10, 0

    digit_print                     db "%d", 10, 0

section .bss
    size_array:         resb 8      ; количество элементов массива
    offset_row_max:     resb 4096   ; максимальный адрес смещения
    offset_row:         resb 4096   ; переменная для смещения в моменте
    random_array:       resb 4096   ; массив для рандомных чисел
    result_array:       resb 4096
    total_size_array:   resb 8


SECTION .text
global create_array_and_perform_transposition
create_array_and_perform_transposition:
    push rbp ;setup stack

    lea rdi, [size_array_message]    ; вывод сообщения о размере массива
    xor rax, rax
    call printf

    lea rdi, [size_array_value_string]     ; заполнения переменной размера массива
    lea rsi, [size_array]
    xor rax, rax
    call scanf

    xor rax, rax
    mov rdx, [size_array]
    mov rax, 4
    mul rdx
    mov [offset_row], rax           ; вычисление смещения для строки (3*3 смещение - 4 * 3 = 12 байт) 

    xor rax, rax
    mov rax, [size_array]
    mul rax
    xor rdx, rdx
    mov rdx, 4
    mul rdx                             ; вычисление смещения для строки (для 3*3 - 3*3*4 = 36 байт) 
    mov [offset_row_max], rax
    
    ; заполнение массива случайными числами
    mov r13, 0 ; counter
    mov r14, 0 ; row
    mov r15, 0 ; column
    for_start:

        for_internal:
            rdtscp
            and rax, 0x0000000000000fff
            mov [random_array + r14 + r15], rax        ; запись рандомного числа в массив

            add r15, 4
            cmp r15, [offset_row]                      ; если указатель для столбца на последнем элементе
            jne for_internal
            jmp for_internal_end
        
        for_internal_end:
            add r14, [offset_row]                      ; новый адрес строки
            mov r15, 0


        add r13, 4
        cmp r14, [offset_row_max]                       ; условие выхода из цикла
        jne for_start
        jmp for_end

    for_end:

    lea rdi, [message_random_array]                     ; вывести сообщение "========== Random array =========="
    xor rax, rax
    call printf

    ; вывести первый массив из рандомых чисел
    mov r13, 0                                          ; счетчик для цикла
    start_print_array:
        mov rax, [size_array]
        add rax, rax
        mul rax
        cmp r13, rax                                    ; условие для выхода из цикла
        je end_print_array

        mov r8, [random_array+r13]
        ; вывод элемента массива
        lea rdi, [digit_print]
        mov rsi, r8
        xor rax, rax
        call printf

        add r13, 4
        jmp start_print_array

    end_print_array:


    lea rdi, [message_result_array]                     ; вывод "========== New array =========="
    xor rax, rax
    call printf

    ; выполнение транспонирования и вывод транспонированной матрицы
    xor r8, r8   ; счетчик для заполнения массива транспонированной матрицы
    xor rax, rax
    mov rax, [size_array]
    mov rcx, [size_array]
    mul rcx
    mov [total_size_array], rax
    xor r14, r14 ; счетчик для строки
    xor r15, r15 ; счетчик для столбца
    for_start_transposition_array:

        for_start_internal_transposition_array:
            xor rax, rax
            mov rax, r14
            add rax, r15
            mov rcx, [random_array + rax * 4]           ; здесь "транспонированный" элемент
            mov [result_array + r8 * 4], rcx            ; записываем транспонированный элемент в новый массив

            lea rdi, [digit_print]                      ; вывод транспонированного элемента
            mov rsi, rcx
            xor rax, rax
            call printf

            add r15, [size_array]
            cmp r15, [total_size_array]
            jne for_start_internal_transposition_array
            xor r15, r15
        for_end_internal_transposition_array:

        add r14, 1
        cmp r14, [size_array]
        jne for_start_transposition_array
    for_end_transposition_array:


    mov     rax, 60         ; системный вызов для завершения работы
    xor     rdi, rdi
    syscall