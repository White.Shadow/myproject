<?php
  session_start();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="styleInd.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;800&display=swap" rel="stylesheet">
    <title>Пицца ZaurJones</title>
  </head>
  <body>

    <div class='header'>
      <div class='container'>
      <div class='header-line'>
        <div class='header-logo'>
          <img src="img\logo-picca.png" width="120" height="130" alt="">
        </div>
        <div class="namePizza">
          <h2>Pizza ZaurJones</h2>
        </div>
        <div class="nav">
          <a class="nav-item" href="/home.php" >ГЛАВНАЯ</a>
          <a class="nav-item" href="/menu.php">МЕНЮ</a>
          <a class="nav-item" href="/reservation.php">БРОНЬ</a>
          <a class="nav-item" href="/history.php">О НАС</a>
        </div>
        <div class="cart">
          <a href="#">
            <img class="cart-img" src="img\cart.png" alt="" title="Корзина">
          </a>
        </div>
        <div class="phone">
          <div class="phone-holder">
          <div class="phone-img">
            <img src="img\phone-cat.png" width="70" height="70" alt="">
          </div>
          <div class="number"><a  class="num" href="#">+7923-223-44-55</a>
          </div>
          </div>
          <div class="phone-text">
            Свяжитесь с нами <br>для бронирования
          </div>
        </div>
        <div class="btn-order">
          <a class="button-order" href="/tableOrder.php">Заказ столика</a>
        </div>
        <?php
          if(is_null($_SESSION['user'])) {
            echo '<div class="btn-auth"> <a class="button-auth" href="/auth.php">Войти</a></div>';
          }
          else
            echo '<div class="btn-auth"> <a class="button-auth" href="/profile.php">Профиль</a></div>';
          ?>
      </div>
      <div class="block-form">
        <iframe  style="border :0px; " target="_top" src="blocks/menu.php" width="73%" height="603" scrolling="yes"></iframe>
      </div>
      </div>
     </div>




  </body>
</html>
