﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dataaccess
{
    public partial class UserAccess : Form
    {
        NpgsqlConnection conn;
        private string FileName = "";
        private int id = 0;

        public UserAccess(string FileName, NpgsqlConnection conn, int ID)
        {
            this.FileName = FileName;
            this.conn = conn;
            this.id = ID;
            InitializeComponent();
            textBox1.KeyPress += (sender, e) => e.Handled = true;

        }

        private void UserAccess_Load(object sender, EventArgs e)
        {
            var command = new NpgsqlCommand();
            command.Connection = conn;
            command.CommandText = $"select * from auth";
            textBox1.Text = FileName;
            NpgsqlDataReader read = command.ExecuteReader();
            while (read.Read())
            {
                checkedListBox1.Items.Add(read.GetString(1));
            }
            read.Close();
        }

        private void checkBox5_Click(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
            {
                checkBox1.Checked = true;
                checkBox2.Checked = true;
                checkBox3.Checked = true;
                checkBox4.Checked = false;
            }
            else
            {
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox3.Checked = false;
            }
        }

        private void checkBox4_Click(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
            {
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox3.Checked = false;
                checkBox5.Checked = false;
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            if (checkBox3.Checked && checkBox1.Checked && checkBox2.Checked)
            {
                checkBox5.Checked = true;
                checkBox4.Checked = false;
            }
            else if (!checkBox3.Checked && !checkBox1.Checked && !checkBox2.Checked)
            {
                checkBox4.Checked = true;
                checkBox5.Checked = false;
            }
            else if(checkBox1.Checked)
            {
                checkBox4.Checked = false;
            }
            else if(!checkBox1.Checked)
            {
                checkBox5.Checked = false;
            }
        }

        private void checkBox2_Click(object sender, EventArgs e)
        {
            if (checkBox3.Checked && checkBox1.Checked && checkBox2.Checked)
            {
                checkBox5.Checked = true;
                checkBox4.Checked = false;
            }
            else if (!checkBox3.Checked && !checkBox1.Checked && !checkBox2.Checked)
            {
                checkBox4.Checked = true;
                checkBox5.Checked = false;
            }
            else if (checkBox2.Checked)
            {
                checkBox4.Checked = false;
            }
            else if (!checkBox2.Checked)
            {
                checkBox5.Checked = false;
            }
        }

        private void checkBox3_Click(object sender, EventArgs e)
        {
            if (checkBox3.Checked && checkBox1.Checked && checkBox2.Checked)
            {
                checkBox5.Checked = true;
                checkBox4.Checked = false;
            }
            else if(!checkBox3.Checked && !checkBox1.Checked && !checkBox2.Checked)
            {
                checkBox4.Checked = true;
                checkBox5.Checked = false;
            }
            else if (checkBox3.Checked)
            {
                checkBox4.Checked = false;
            }
            else if (!checkBox3.Checked)
            {
                checkBox5.Checked = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count != 0)
            {
                for (int i = 0; i < checkedListBox1.CheckedItems.Count; i++)
                {
                    var command = new NpgsqlCommand();
                    command.Connection = conn;
                    command.CommandText = $"DELETE FROM useraccess where id_username = '{checkedListBox1.CheckedItems[i].ToString()}' and filename = '{FileName}'";
                    command.ExecuteNonQuery();

                    var sql = "INSERT INTO public.useraccess(filename, readfile, readwritefile, accomplishment, \"access\", id_username) " +
                        "VALUES(@Filename, @readFile, @Readwritefile, @Accomplishment, @Access, @Id_username); ";
                    var cmd = new NpgsqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("Filename", textBox1.Text);
                    cmd.Parameters.AddWithValue("readFile", checkBox1.Checked.ToString());

                    cmd.Parameters.AddWithValue("Readwritefile", checkBox2.Checked.ToString());
                    cmd.Parameters.AddWithValue("Accomplishment", checkBox3.Checked.ToString());

                    cmd.Parameters.AddWithValue("Access", checkBox4.Checked.ToString());
                    cmd.Parameters.AddWithValue("Id_username", checkedListBox1.CheckedItems[i].ToString());

                    cmd.Prepare();

                    cmd.ExecuteNonQuery();
                }
                MessageBox.Show("Права доступа выданы!", "Выполнено!");
            }
            else MessageBox.Show("Выберите пользователя!", "Ошибка!");
        }

        private void UserAccess_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            new DataAccess(id, conn).Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            new DataAccess(id, conn).Show();
        }
    }
}
