﻿using Npgsql;
using System;
using System.Windows.Forms;

namespace Dataaccess
{
    public partial class Registration : Form
    {
        private NpgsqlConnection conn;
        public Registration(NpgsqlConnection Conn)
        {
          conn = Conn;
          InitializeComponent();
            comboBox1.KeyPress += (sender, e) => e.Handled = true;
        }

        private void RegBut_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "Имя пользователя" || textBox2.Text == "Пароль" || textBox3.Text == "Повторите пароль" || comboBox1.Text == "Роль"|| textBox2.Text != textBox3.Text)
            {
                MessageBox.Show("Заполните данные правильно!!!");
            }
            else
            {
                bool check = Program.CheckUser(conn, textBox1.Text);
                if (check == false)
                {
                    var sql = "INSERT INTO auth(login, password, role, checkrole) VALUES(@log, @passwd, @role, @checkrole)";
                    var cmd = new NpgsqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("log", textBox1.Text);
                    cmd.Parameters.AddWithValue("passwd", textBox2.Text);
                    if (comboBox1.Text == "Пользователь")
                    {
                        cmd.Parameters.AddWithValue("role", "user");
                        cmd.Parameters.AddWithValue("checkrole", "true");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("role", "admin");
                        cmd.Parameters.AddWithValue("checkrole", "check");
                    }
                    cmd.Prepare();

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Вы успешно зарегистрировались!", "Выполнено!");
                }
                else MessageBox.Show("Такой пользователь уже существует", "Ошибка регистрации!");
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Authorization().Show();
        }


        private void RegFormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void textbox1_click(object sender, EventArgs e)
        {
            if (textBox1.Text == "Имя пользователя")
                textBox1.Clear();
        }

        private void textbox1_leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
                textBox1.Text = "Имя пользователя";
        }

        private void textbox2_click(object sender, EventArgs e)
        {
            if (textBox2.Text == "Пароль")
                textBox2.Clear();
            textBox2.PasswordChar = '*';
        }

        private void textbox2_leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                textBox2.Text = "Пароль";
                textBox2.PasswordChar = '\0';
            }
            else
            {
                textBox2.PasswordChar = '*';
            }
        }

        private void textbox3_leave(object sender, EventArgs e)
        {
            if (textBox3.Text == "")
            {
                textBox3.Text = "Повторите пароль";
                textBox3.PasswordChar = '\0';
            }
            else
            {
                textBox3.PasswordChar = '*';
            }
        }
        private void textbox3_click(object sender, EventArgs e)
        {
            if (textBox3.Text == "Повторите пароль")
                textBox3.Clear();
            textBox3.PasswordChar = '*';
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Registration_Load(object sender, EventArgs e)
        {

        }
    }
}