﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dataaccess
{
    public partial class OpenFiles : Form
    {
        private NpgsqlConnection conn;
        private int IdUser = 0;
        private string FileName = "";
        private string readfile = "";

        public OpenFiles(NpgsqlConnection conn, int IdUser, string FileName, string readfile)
        {
            this.conn = conn;
            this.IdUser = IdUser;
            this.FileName = FileName;
            this.readfile = readfile;
            InitializeComponent();
        }

        private void OpenFiles_Load(object sender, EventArgs e)
        {
            label1.Text += FileName;
            if (readfile != "True")
                label2.Text += "Чтение и запись!";
            else label2.Text += "Только для чтения";

            textBox1.Text = File.ReadAllText(FileName);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (readfile != "True")
            {
                File.WriteAllText(FileName,  textBox1.Text);
                MessageBox.Show("Изменения сохранены!", "Выполнено!");
            }
            else MessageBox.Show("Невозможно сохранить документ!", "Ограничение прав доступа");
        }

        private void OpenFiles_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            new DataAccess(IdUser, conn).Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            new DataAccess(IdUser, conn).Show();
        }
    }
}
