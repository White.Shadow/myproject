﻿using System;
using System.Windows.Forms;
namespace Communication
{
    public partial class Phonebook : Form
    {
        string Id;
        public Phonebook(string id)
        {
            InitializeComponent();
            Id = id;
        }

        ConnectClient connectClient = new ConnectClient();

        private void Phonebook_Load(object sender, EventArgs e)
        {
            textBox3.ScrollBars = ScrollBars.Both;
            textBox3.Clear();
            int  s = 2;
            string Message = connectClient.ConServer("3|4" + Id + "|" + textBox2.Text + "|" + textBox1.Text);
            if (Message == "false")
                textBox3.Text += "Нет подходящих результатов!";
            else if (Message != "0")
            {
                textBox3.Text = "1. Имя:";
                for (int i = 0; i < Message.Length-1; i++)
                {
                    if (Message[i] == '|')
                    {
                        s++;
                        if (s % 2 != 0) 
                            textBox3.Text += "\r\n    Номер:";
                        else if (s % 2 == 0)
                        {
                            textBox3.Text += "\r\n-------------------------------------------------------------------------------------";

                            textBox3.Text += "\r\n" + (s / 2).ToString() + ". Имя:";
                        }
                 
                    }
                    else
                    {
                        textBox3.Text += Message[i];
                    }
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text != "") && (textBox2.Text != ""))
            {
                string Message = connectClient.ConServer("3|3" + Id +"|"+ textBox2.Text + "|" + textBox1.Text);
                MessageBox.Show("Номер добавлен!");
            }
            else
                MessageBox.Show("Имя или номер не может быть пустым!");

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox3.ScrollBars = ScrollBars.Both;
            textBox3.Clear();
            int s = 2;
            string Message = connectClient.ConServer("3|4" + Id + "|" + textBox2.Text + "|" + textBox1.Text);
            if (Message == "false")
                textBox3.Text += "Нет подходящих результатов!";
            else if (Message != "0")
            {
                textBox3.Text = "1. Имя:";
                for (int i = 0; i < Message.Length - 1; i++)
                {
                    if (Message[i] == '|')
                    {
                        s++;
                        if (s % 2 != 0)
                            textBox3.Text += "\r\n    Номер:";
                        else if (s % 2 == 0)
                        {
                            textBox3.Text += "\r\n-------------------------------------------------------------------------------------";

                            textBox3.Text += "\r\n" + (s / 2).ToString() + ". Имя:";
                        }

                    }
                    else
                    {
                        textBox3.Text += Message[i];
                    }
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar == '|') || !Char.IsDigit(e.KeyChar) && (e.KeyChar != 8))
            {
                e.Handled = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar == '|'))
            {
                e.Handled = true;
            }
        }
    }
}

