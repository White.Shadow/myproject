using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kr2;
using System.Data.SQLite;

namespace TestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]

        public void TestPhonebook()
        {
            //arrange
            string Data = "3|3840|2348234982|Zaurovd";
            string expected = "0";

            // act
            SQLiteConnection DB = new SQLiteConnection("Data Source=C:/Users/Zaur Velibekov/kursach_pain/Final_KR_TIMP/Server/Kr2/bin/Debug/BdOrganazer.db; Version=3");
            //SQLiteConnection DB = new SQLiteConnection("Data Source=BdOrganazer.db; Version=3");

            DB.Open();
            PhonebookSer phonebook = new PhonebookSer();
            string actual = phonebook.NumFio(Data, DB);
            //assert

            Assert.AreEqual(expected, actual);
        }
    }
}
