<?php
  session_start();
  require_once '../vendor/connect.php';
  if(!$_SESSION['user']) {
    $_SESSION['message'] = 'Авторизуйтесь, для заказа столика!';
    header('Location: ../auth.php');
  }
?>

<!DOCTYPE HTML>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="../css/reservation.css">
  <title>Бронь</title>
  <style type="text/css">

  body::-webkit-scrollbar {
    width: 0px;               /* ширина всей полосы прокрутки */
  }

  body {
    background: black; /* А это работает */
    background: rgba(0, 0, 0, 0.5);
  }

  body::-webkit-scrollbar-track {
    background: orange;        /* цвет зоны отслеживания */
  }

  body::-webkit-scrollbar-thumb {
    background-color: blue;    /* цвет бегунка */
    border-radius: 20px;       /* округлось бегунка */
    border: 3px solid orange;  /* отступ вокруг бегунка */
  }
</style>
 </head>
 <body>

  <!-- Бронь -->
  <?php
    if($_SESSION['message'] == "Ваш стол забронирован!") {
      echo '<div class="alert success"><span class="closebtn">&times;</span><strong>Отлично! </strong>' . $_SESSION['message'] . '  </div>';
    }
    unset($_SESSION['message']);
    $login =  $_SESSION['user']['login'];
    $check_user = mysqli_query($connect, "SELECT * FROM `tableOrder` WHERE `login` = '$login'");
    if(mysqli_num_rows($check_user) > 0) {
      $user = mysqli_fetch_assoc($check_user);
      $_SESSION['tableOrder'] = [
        "login" =>$user['login'],
        "email" =>$_SESSION['user']['email'],
        "tel" =>$user['tel'],
        "date" =>$user['date'],
        "time" =>$user['time'],
        "comment" =>$user['comment']
      ];
  }
  else {
    $_SESSION['tableOrder'] = [
      "login" =>$_SESSION['user']['login'],
      "email" =>$_SESSION['user']['email'],
      "tel" =>"Мы не знаем вашего номера:( ",
      "date" => "любое рабочее время",
      "time" =>"удобный для вас день:)",
      "comment" => "Всегда накормим вас с любовью! к сожалению, у вас нет забронированных столиков,
      но можете заказать столик на нужное вам время по номеру,
      указанному на панели быстрого доступа либо на сайте через кнопку ЗАКАЗ СТОЛИКА) Ждем вас))"
    ];
  }
  ?>
  <div class="" style="margin-left: 10px; color:white; font-size:28px;">
    <h1 style="text-align:center;">ВАША БРОНЬ:</h1>
    <h3>Логин: <?php echo $_SESSION['tableOrder']['login'] ?></h3>
      <h3>Почта: <?php echo $_SESSION['tableOrder']['email'] ?></h3>
        <h3>Номер телефона:<?php echo $_SESSION['tableOrder']['tel'] ?></h3>
          <h3>Столик забронирован на:<?php echo $_SESSION['tableOrder']['date'] .' в ' . $_SESSION['tableOrder']['time'] ?></h3>
          <h3 style=" text-align: justify;">Пожелания:<?php echo $_SESSION['tableOrder']['comment']?></h3>
  </div>

<script>
var close = document.getElementsByClassName("closebtn");
var i;

for (i = 0; i < close.length; i++) {
  close[i].onclick = function(){
    var div = this.parentElement;
    div.style.opacity = "0";
    setTimeout(function(){ div.style.display = "none"; }, 600);
  }
}
</script>
 </body>
</html>
