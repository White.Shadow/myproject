﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kr2
{
    public class PhonebookSer
    {
        public string phone;
        public string NumFio(string Data, SQLiteConnection db)
        {
            SQLiteCommand command = db.CreateCommand();
            string Nomer = "", Fio = "", message = "0", id = "";
            int K = 0;
            for (int i = 3; i < Data.Length; i++)
            {
                if (Data[i] == '|')
                {
                    K++;
                }
                else if (K == 0)
                {
                    id += Data[i];
                }
                else if (K == 1)
                    Nomer += Data[i];
                else Fio += Data[i];

            }
            phone = id;
            if (Data[2] == '3')
            {
                command.Parameters.Add("@Nomer", DbType.String).Value = Nomer.ToUpper();
                command.Parameters.Add("@Fio", DbType.String).Value = Fio.ToUpper();
                command.Parameters.Add("@Id", DbType.String).Value = id;
                //command.CommandText = "select * from Телефоны where Номер like @Nomer";
                command.CommandText = "insert into Телефоны(ИД, Номер, ФИО) values(@Id, @Nomer, @Fio)";
                command.ExecuteNonQuery();
            }
            else if (Data[2] == '4')
            {
                message = "";
                command.CommandText = "select * from Телефоны where  Номер like '%' || @Nomer || '%' and ФИО like '%' || @Fio || '%' and ИД like @Id ";
                command.Parameters.Add("@Nomer", DbType.String).Value = Nomer.ToUpper();
                command.Parameters.Add("@Fio", DbType.String).Value = Fio.ToUpper();
                command.Parameters.Add("@Id", DbType.String).Value = id;
                SQLiteDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        message += reader["ФИО"] + "|" + reader["Номер"] + "|";
                    }

                }
                else message = "false";
            }
            return message;
        }
        public int ddd(string data)
        {
            return 1 + 2;
        }
    }
}
